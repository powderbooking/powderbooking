# powderbooking

Application to show the best hotel deals with the best powdersnow

### Environmental variables

The CI/CD pipeline expects the following environmental variables to be set:

| Variable                        | Default | Masked | Description                                                                  |
|---------------------------------|---------|--------|------------------------------------------------------------------------------|
| WEATHERUNLOCKED_APP_ID          |         | +      | The ID for the weather unlocked API                                          |
| WEATHERUNLOCKED_APP_KEY         |         | +      | The key for the weather unlocked API                                         |
| OPENWEATHERMAP_APP_ID           |         | +      | The ID for the open weather map API                                          |
| POSTGRESQL_PASSWORD             |         | +      | The password for the postgresql database                                     |
| POSTGRESQL_PASSWORD_ADMIN       |         | +      | The password for the postgresql database postgres super user                 |
| POSTGRESQL_PASSWORD_REPLICATION |         | +      | The password for the postgresql database replication user (for future use)   |
| ENABLE_PROMETHEUS               | true    |        | Whether prometheus monitoring is enabled or not                              |
| EMAIL                           |         | +      | The email address used for let's encrypt to warn about possible TLS changes. |
