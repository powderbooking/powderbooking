{{/*
Expand the name of the chart.
*/}}
{{- define "powderbooking-readme.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "powderbooking-readme.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "powderbooking-readme.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
List the labels related to the project
*/}}
{{- define "labels.project" -}}
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/part-of: {{ .Chart.Name }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
helm.sh/chart: {{ include "powderbooking-readme.chart" . }}
{{- end -}}

{{/*
List the labels related to the readme
*/}}
{{- define "labels.readme" -}}
{{ include "labels.project" . }}
app.kubernetes.io/name: {{ include "powderbooking-readme.name" . }}
app.kubernetes.io/component: readme
app.kubernetes.io/version: {{ .Values.image.tag }}
{{- end -}}
{{- define "labels.readme.selectorLabels" -}}
app.kubernetes.io/name: {{ include "powderbooking-readme.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}