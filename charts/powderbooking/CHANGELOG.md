# Changelog


## v0.5.0

### Changed

Moved postgresql outside of the Helm chart. It is now expected to be present in the cluster.

## v0.4.0

### Added
- CHANGELOG
- scraper v0.4.0
- Add configuration to the scraper
- Add scraper metrics through pushgateway

### Changed
- Moved `scraper.weather` and `scraper.forecast` to `scraper.cronjob` in `values.yaml`.