environment: local

fullnameOverride: ""
nameOverride: ""

# These are secrets to connect to the APIs to scrape the data
weatherUnlockedAppId: your-secret-here
weatherUnlockedAppKey: your-secret-here
openWeatherMapAppId: your-secret-here

prometheusEnabled: true

# This is the host for the prometheus pushgateway to push the cronjob metrics
prometheusPushgateway: "http://prometheus-pushgateway.prometheus:9091"

containerRegistry: registry.gitlab.com/powderbooking

clusterIssuer: letsencrypt-cluster-issuer

backend:
  replicaCount: 1

  image:
    repo: powderbooking-backend-dotnetcore
    tag: v0.3.0
    pullPolicy: IfNotPresent

  service:
    port: 80

  ingress:
    enabled: false
    hosts:
      - host: api.powderbooking.com
        paths:
          - /

  resources:
    limits:
      cpu: 100m
      memory: 250Mi
    requests:
      memory: 100Mi
      cpu: 10m

frontend:
  replicaCount: 1

  image:
    repo: powderbooking-nextjs
    tag: v0.3.0
    pullPolicy: IfNotPresent

  service:
    port: 80

  ingress:
    enabled: true
    hosts:
      - host: powderbooking.com
        paths:
          - /

  resources:
    limits:
      cpu: 200m
      memory: 300Mi
    requests:
      memory: 150Mi
      cpu: 100m

  weatherType: snow

scraper:
  image:
    repo: powderbooking-scraper
    tag: v0.6.0
    pullPolicy: IfNotPresent
  restartPolicy: Never

  cronjob:
    weather:
      enabled: true
      # At minute 1 past every 6th hour
      schedule: "1 */6 * * *"
    forecast:
      enabled: true
      # At 01:01 on every day-of-month
      schedule: "1 1 */1 * *"

  # The boundaries of a randomized delay between two API calls (in seconds)
  delay_min: 0.4
  delay_max: 1.0
  # The delay after an error (in seconds)
  delay_error: 2.0
  # After how many API calls we will insert into the database
  queue_max: 20
  failure_threshold: 10
  # The amount of consecutive errors are accepted before the circuit is broken

  resources:
    limits:
      cpu: 100m
      memory: 100Mi
    requests:
      memory: 30Mi
      cpu: 25m

bootstrap:
  enabled: false
  args: ["-f", "resort_data.sql"]

  image:
    repo: powderbooking-bootstrap
    tag: v0.2.2
    pullPolicy: IfNotPresent
  restartPolicy: Never

bootstrapDynamic:
  enabled: false

  image:
    repo: powderbooking-bootstrap-dynamic
    tag: v0.5.1
    pullPolicy: IfNotPresent
  restartPolicy: Never

  args: ""
  # At minute 5 and 35 of every hour
  schedule: "5,35 * * * *"

  resources:
    limits:
      memory: 10Mi
      cpu: 10m
    requests:
      memory: 10Mi
      cpu: 10m

postgresql:
  auth:
    database: powderbooking
    username: powderbooking
    password: ""
  primary:
    service:
      ports:
        postgresql: 5432
