{{/* vim: set filetype=mustache: */}}

{{/*
List the environmental variables related to the project
*/}}
{{- define "env.project" -}}
- name: PROJECT_NAME
  valueFrom:
    configMapKeyRef:
      name: {{ include "configuration.fullname" . }}
      key: PROJECT_NAME
- name: PROJECT_ENVIRONMENT
  valueFrom:
    configMapKeyRef:
      name: {{ include "configuration.fullname" . }}
      key: PROJECT_ENVIRONMENT
{{- end -}}

{{/*
List the environmental variables related to postgresql
*/}}
{{- define "env.postgresql" -}}
- name: POSTGRESQL_HOST
  value: {{ include "postgresql.fullname" . }}
- name: POSTGRESQL_PORT
  value: {{ .Values.postgresql.primary.service.ports.postgresql | quote }}
- name: POSTGRESQL_DATABASE
  valueFrom:
    configMapKeyRef:
      name: {{ include "configuration.fullname" . }}
      key: POSTGRESQL_DATABASE
- name: POSTGRESQL_USER
  valueFrom:
    configMapKeyRef:
      name: {{ include "configuration.fullname" . }}
      key: POSTGRESQL_USER
- name: POSTGRESQL_PASSWORD
  valueFrom:
    secretKeyRef:
      name: {{ include "postgresql.fullname" . }}
      key: password
- name: WAIT_HOSTS
  value: "{{ include "postgresql.fullname" . }}:{{ .Values.postgresql.primary.service.ports.postgresql }}"
{{- end -}}

{{/*
List the environmental variables related to the scraper
*/}}
{{- define "env.backend.consumer" -}}
- name: BACKEND_API
  value: "http://{{ include "backend.fullname" . }}:{{ .Values.backend.service.port }}"
{{- end -}}

{{/*
List the environmental variables related to the scraper
*/}}
{{- define "env.scraper" -}}
- name: WEATHERUNLOCKED_APP_ID
  valueFrom:
    secretKeyRef:
      name: {{ include "scraper.secret.fullname" . }}
      key: WEATHERUNLOCKED_APP_ID
- name: WEATHERUNLOCKED_APP_KEY
  valueFrom:
    secretKeyRef:
      name: {{ include "scraper.secret.fullname" . }}
      key: WEATHERUNLOCKED_APP_KEY
- name: OPENWEATHERMAP_APP_ID
  valueFrom:
    secretKeyRef:
      name: {{ include "scraper.secret.fullname" . }}
      key: OPENWEATHERMAP_APP_ID
{{/*
# - name: PROMETHEUS_PUSHGATEWAY
#   valueFrom:
#     configMapKeyRef:
#       name: {{ include "configuration.fullname" . }}
#       key: PROMETHEUS_PUSHGATEWAY
*/}}
- name: DELAY_MIN
  valueFrom:
    configMapKeyRef:
      name: {{ include "configuration.fullname" . }}
      key: SCRAPER_DELAY_MIN
- name: DELAY_MAX
  valueFrom:
    configMapKeyRef:
      name: {{ include "configuration.fullname" . }}
      key: SCRAPER_DELAY_MAX
- name: DELAY_ERROR
  valueFrom:
    configMapKeyRef:
      name: {{ include "configuration.fullname" . }}
      key: SCRAPER_DELAY_ERROR
- name: QUEUE_MAX
  valueFrom:
    configMapKeyRef:
      name: {{ include "configuration.fullname" . }}
      key: SCRAPER_QUEUE_MAX
- name: FAILURE_THRESHOLD
  valueFrom:
    configMapKeyRef:
      name: {{ include "configuration.fullname" . }}
      key: SCRAPER_FAILURE_THRESHOLD
{{- end -}}
