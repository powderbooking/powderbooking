{{/* vim: set filetype=mustache: */}}

{{/*
---------------------
GENERAL NAMES
---------------------
*/}}

{{/*
Expand the name of the chart.
*/}}
{{- define "powderbooking.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "powderbooking.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "powderbooking.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}


{{/*
---------------------
MICRO-SERVICE NAMES
---------------------
*/}}

{{- define "backend.name" -}}backend{{- end -}}
{{- define "backend.fullname" -}}
{{ include "powderbooking.name" . -}}-{{- include "backend.name" . }}
{{- end -}}

{{- define "configuration.name" -}}configuration{{- end -}}
{{- define "configuration.fullname" -}}
{{ include "powderbooking.name" . -}}-{{- include "configuration.name" . }}
{{- end -}}

{{- define "frontend.name" -}}frontend{{- end -}}
{{- define "frontend.fullname" -}}
{{ include "powderbooking.name" . -}}-{{- include "frontend.name" . }}
{{- end -}}

{{- define "scraper.forecast.name" -}}scraper-forecast{{- end -}}
{{- define "scraper.forecast.fullname" -}}
{{ include "powderbooking.name" . -}}-{{- include "scraper.forecast.name" . }}
{{- end -}}

{{- define "scraper.weather.name" -}}scraper-weather{{- end -}}
{{- define "scraper.weather.fullname" -}}
{{ include "powderbooking.name" . -}}-{{- include "scraper.weather.name" . }}
{{- end -}}

{{- define "scraper.secret.name" -}}scraper-secret{{- end -}}
{{- define "scraper.secret.fullname" -}}
{{ include "powderbooking.name" . -}}-{{- include "scraper.secret.name" . }}
{{- end -}}

{{- define "bootstrap.name" -}}bootstrap{{- end -}}
{{- define "bootstrap.fullname" -}}
{{ include "powderbooking.name" . -}}-{{- include "bootstrap.name" . }}
{{- end -}}

{{- define "bootstrap.dynamic.name" -}}bootstrap-dynamic{{- end -}}
{{- define "bootstrap.dynamic.fullname" -}}
{{ include "powderbooking.name" . -}}-{{- include "bootstrap.dynamic.name" . }}
{{- end -}}

{{- define "issuer.name" -}}letsencrypt-issuer{{- end -}}

{{- define "postgresql.fullname" -}}postgresql{{- end -}}