{{/* vim: set filetype=mustache: */}}

{{/*
---------------------
GENERAL LABELS
ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/common-labels/
---------------------
*/}}

{{- define "labels.release" -}}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}

{{- define "labels.project" -}}
{{ include "labels.release" . }}
app.kubernetes.io/part-of: {{ include "powderbooking.chart" . }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
helm.sh/chart: {{ include "powderbooking.chart" . }}
{{- end -}}


{{/*
---------------------
MICRO-SERVICE LABELS
---------------------
*/}}

{{- define "labels.backend" -}}
{{ include "labels.project" . }}
app.kubernetes.io/name: {{ include "backend.fullname" . }}
app.kubernetes.io/component: {{ include "backend.name" . }}
app.kubernetes.io/version: {{ .Values.backend.image.tag }}
{{- end -}}
{{- define "labels.backend.selectorLabels" -}}
{{ include "labels.release" . }}
app.kubernetes.io/name: {{ include "backend.fullname" . }}
{{- end -}}

{{- define "labels.configuration" -}}
{{ include "labels.project" . }}
app.kubernetes.io/name: {{ include "configuration.fullname" . }}
app.kubernetes.io/component: {{ include "configuration.name" . }}
app.kubernetes.io/version: {{ .Chart.Version }}
{{- end -}}

{{- define "labels.frontend" -}}
{{ include "labels.project" . }}
app.kubernetes.io/name: {{ include "frontend.fullname" . }}
app.kubernetes.io/component: {{ include "frontend.name" . }}
app.kubernetes.io/version: {{ .Values.frontend.image.tag }}
{{- end -}}
{{- define "labels.frontend.selectorLabels" -}}
{{ include "labels.release" . }}
app.kubernetes.io/name: {{ include "frontend.fullname" . }}
{{- end -}}

{{- define "labels.scraper.forecast" -}}
{{ include "labels.project" . }}
app.kubernetes.io/name: {{ include "scraper.forecast.fullname" . }}
app.kubernetes.io/component: {{ include "scraper.forecast.name" . }}
app.kubernetes.io/version: {{ .Values.scraper.image.tag }}
{{- end -}}

{{- define "labels.scraper.weather" -}}
{{ include "labels.project" . }}
app.kubernetes.io/name: {{ include "scraper.weather.fullname" . }}
app.kubernetes.io/component: {{ include "scraper.weather.name" . }}
app.kubernetes.io/version: {{ .Values.scraper.image.tag }}
{{- end -}}

{{- define "labels.scraper.secret" -}}
{{ include "labels.project" . }}
app.kubernetes.io/name: {{ include "scraper.secret.fullname" . }}
app.kubernetes.io/component: {{ include "scraper.secret.name" . }}
app.kubernetes.io/version: {{ .Chart.Version }}
{{- end -}}

{{- define "labels.bootstrap" -}}
{{ include "labels.project" . }}
app.kubernetes.io/name: {{ include "bootstrap.fullname" . }}
app.kubernetes.io/component: {{ include "bootstrap.name" . }}
app.kubernetes.io/version: {{ .Values.bootstrap.image.tag }}
{{- end -}}

{{- define "labels.bootstrap.dynamic" -}}
{{ include "labels.project" . }}
app.kubernetes.io/name: {{ include "bootstrap.dynamic.fullname" . }}
app.kubernetes.io/component: {{ include "bootstrap.dynamic.name" . }}
app.kubernetes.io/version: {{ .Values.bootstrap.image.tag }}
{{- end -}}


{{/*
---------------------
ANNOTATIONS
---------------------
*/}}

{{- define "annotations.ingress" -}}
kubernetes.io/ingress.class: nginx
cert-manager.io/cluster-issuer: {{ .Values.clusterIssuer }}
{{- end -}}